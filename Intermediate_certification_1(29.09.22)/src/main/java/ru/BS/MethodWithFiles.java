package ru.BS;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
public class MethodWithFiles {
    // метод nameFile() - указать файл (полный путь) для работы с ним
    public static String nameFile(){
        Scanner scannerPath = new Scanner(System.in);
        System.out.print("Введите полный путь к файлу, пример (D:\\Имя файла.txt)" + "\n" +"-> ");
        return scannerPath.next();
    }
    // метод creatFile() - проверяет файл на его наличие, при отрицательном поиске создает файл
    public static String creatFile (){
        Scanner scannerPath = new Scanner(System.in);
        System.out.print("Введите полный путь к файлу, пример (D:\\Имя файла.txt), в который будет добавленна информация после преобразования" + "\n" +"-> ");
        String newNameFile = scannerPath.next();
        File file = new File(newNameFile);
        try {
            if (file.createNewFile()) System.out.println("Файл создан");
            else System.out.println("Файл с таким именем уже существует");
        } catch (IOException e) {
            System.out.println("Ошибка при создании файла");
            throw new RuntimeException(e);
        }
        return newNameFile;
    }
}
