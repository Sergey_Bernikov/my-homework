package ru.BS;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
public class WorkingWithFiles {
    // метод readTheWordsInTheText() - чтение файла с последующей записи слов в Map (ключ - слово в тексте, значение - повторяемость в тексте
    static public Map <String, Integer> readTheWordsInTheText (String nameFile, Map <String, Integer> map){
        try (Scanner scanner = new Scanner(new File(nameFile))){
            while (scanner.hasNext()){
                String word = scanner.next().toLowerCase().replaceAll("[^A-Za-z0-9А-ЯЁёа-я]", "");
                 if (!map.containsKey(word)){
                    map.put(word, 1);
                }
                else {
                    Integer wordRepetition = map.get(word);
                    map.put(word, wordRepetition + 1);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка чтения файла");
            throw new RuntimeException(e);
        }
        return map;
    }
    // метод readTheCharactersOfWordsInTheText() - чтение файла с последующей записи символов слов в Map (ключ - символы в тексте, значение - повторяемость в тексте (словах)
    static public Map <Character, Integer> readTheCharactersOfWordsInTheText (String nameFile, Map <Character, Integer> map){

        try (Scanner scanner = new Scanner(new File(nameFile))){
            while (scanner.hasNext()){
                String word = scanner.next().toLowerCase().replaceAll("[^A-Za-z0-9А-ЯЁёа-я]", "");
                for (Character el : word.toCharArray()){
                    if (!map.containsKey(el)){
                        map.put(el, 1);
                    }
                    else {
                        Integer charRepetition = map.get(el);
                        map.put(el, charRepetition + 1);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка чтения файла");
            throw new RuntimeException(e);
        }
        return map;
    }
    // метод recFileMap() - запись в файл содержимого в коллекциях Map
    public static void recFileMap(String nameFile, Map <?, ?> map){
        try (FileWriter fileWriter = new FileWriter(nameFile,true)){
            for (Map.Entry<?,?> entry : map.entrySet()){
                fileWriter.write(entry.getKey() + " - " + entry.getValue() + "\r\n");
            }
        } catch (IOException e) {
            System.out.println("Ошибка записи в файл");
            throw new RuntimeException(e);
        }
    }
}
