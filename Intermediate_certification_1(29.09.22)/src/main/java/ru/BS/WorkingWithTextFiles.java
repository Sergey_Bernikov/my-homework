package ru.BS;

import java.util.*;
public class WorkingWithTextFiles {
    String nameFile;                  // имя файла с текстом
    String newNameFile;               // созданный новый файл
    public static void main(String[] args) {
        WorkingWithTextFiles workingWithTextFiles = new WorkingWithTextFiles();
        workingWithTextFiles.run();
    }
    public void run(){

        /* ** Дополнительное задание 1:
         реализовать возможность передачи пути файла.
         Т.е. при старте приложения, приложение просит указать путь к файлу, в котором находится текст. ** */
        nameFile = MethodWithFiles.nameFile();
        // метод creatFile() - проверяет файл на его наличие, при отрицательном поиске создает файл - файл с результатом !!!!!!!!
        newNameFile = MethodWithFiles.creatFile();
/* ****************************************************************************************************************** */
        // Уровень 1: Подсчет слов
        // создаем коллекцию Map для хранения слов + дополнительное задание 2: сделать результирующую запись в файл отсортированной по алфавиту
        Map <String, Integer> mapWord = new TreeMap<>();
        // метод readTheWordsInTheText() - чтение файла с последующей записи слов в Map (ключ - слово в тексте, значение - повторяемость в тексте
        Map <String, Integer> mapWithWord = WorkingWithFiles.readTheWordsInTheText(nameFile, mapWord);
        // метод recFileMap() - запись в файл содержимого в коллекциях Map
        WorkingWithFiles.recFileMap(newNameFile, mapWithWord);
/* ****************************************************************************************************************** */
        // Уровень 2: Подсчет букв (символов)
        // создаем коллекцию Map для хранения символов + дополнительное задание 2: сделать результирующую запись в файл отсортированной по алфавиту
        Map <Character, Integer> mapChar = new TreeMap<>();
        // метод readTheCharactersOfWordsInTheText() - чтение файла с последующей записи символов слов в Map (ключ - символы в тексте, значение - повторяемость в тексте (словах)
        Map <Character, Integer> mapWithChar = WorkingWithFiles.readTheCharactersOfWordsInTheText(nameFile, mapChar);
        // метод recFileMap() - запись в файл содержимого в коллекциях Map
        WorkingWithFiles.recFileMap(newNameFile, mapWithChar);

    }
}

